use nom::{
    combinator::cond,
    error::ParseError,
    multi::count,
    number::complete::{le_i16, le_u32, u8},
    sequence::tuple,
    Parser,
};

const GUNSHOT: u8 = 0;
const BLOOD: u8 = 1;
const BLASTER: u8 = 2;
const RAILTRAIL: u8 = 3;
const SHOTGUN: u8 = 4;
const EXPLOSION1: u8 = 5;
const EXPLOSION2: u8 = 6;
const ROCKET_EXPLOSION: u8 = 7;
const GRENADE_EXPLOSION: u8 = 8;
const SPARKS: u8 = 9;
const SPLASH: u8 = 10;
const BUBBLETRAIL: u8 = 11;
const SCREEN_SPARKS: u8 = 12;
const SHIELD_SPARKS: u8 = 13;
const BULLET_SPARKS: u8 = 14;
const LASER_SPARKS: u8 = 15;
const PARASITE_ATTACK: u8 = 16;
const ROCKET_EXPLOSION_WATER: u8 = 17;
const GRENADE_EXPLOSION_WATER: u8 = 18;
const MEDIC_CABLE_ATTACK: u8 = 19;
const BFG_EXPLOSION: u8 = 20;
const BFG_BIGEXPLOSION: u8 = 21;
const BOSSTPORT: u8 = 22;
const BFG_LASER: u8 = 23;
const GRAPPLE_CABLE: u8 = 24;
const WELDING_SPARKS: u8 = 25;
const GREENBLOOD: u8 = 26;
const BLUEHYPERBLASTER: u8 = 27;
const PLASMA_EXPLOSION: u8 = 28;
const TUNNEL_SPARKS: u8 = 29;
const BLASTER2: u8 = 30;
const RAILTRAIL2: u8 = 31;
const FLAME: u8 = 32;
const LIGHTNING: u8 = 33;
const DEBUGTRAIL: u8 = 34;
const PLAIN_EXPLOSION: u8 = 35;
const FLASHLIGHT: u8 = 36;
const FORCEWALL: u8 = 37;
const HEATBEAM: u8 = 38;
const MONSTER_HEATBEAM: u8 = 39;
const STEAM: u8 = 40;
const BUBBLETRAIL2: u8 = 41;
const MOREBLOOD: u8 = 42;
const HEATBEAM_SPARKS: u8 = 43;
const HEATBEAM_STEAM: u8 = 44;
const CHAINFIST_SMOKE: u8 = 45;
const ELECTRIC_SPARKS: u8 = 46;
const TRACKER_EXPLOSION: u8 = 47;
const TELEPORT_EFFECT: u8 = 48;
const DBALL_GOAL: u8 = 49;
const WIDOWBEAMOUT: u8 = 50;
const NUKEBLAST: u8 = 51;
const WIDOWSPLASH: u8 = 52;
const EXPLOSION1_BIG: u8 = 53;
const EXPLOSION1_NP: u8 = 54;
const FLECHETTE: u8 = 55;

#[derive(Debug)]
pub struct TempEntity {
    // TODO
}

pub fn temp_entity_parser<'p, E: ParseError<&'p [u8]>>() -> impl Parser<&'p [u8], TempEntity, E> {
    |i: &'p [u8]| {
        let readpos = || count(le_i16, 3);
        let readdir = u8;
        let (i, te_type) = u8(i)?;
        let t1 = matches!(
            te_type,
            BLOOD
                | GUNSHOT
                | SPARKS
                | BULLET_SPARKS
                | SCREEN_SPARKS
                | SHIELD_SPARKS
                | SHOTGUN
                | BLASTER
                | GREENBLOOD
                | BLASTER2
                | FLECHETTE
                | HEATBEAM_SPARKS
                | HEATBEAM_STEAM
                | MOREBLOOD
                | ELECTRIC_SPARKS
        );
        let (i, _pos_dir) = cond(t1, tuple((readpos(), readdir)))(i)?;

        let t2 = matches!(
            te_type,
            SPLASH | LASER_SPARKS | WELDING_SPARKS | TUNNEL_SPARKS,
        );

        let (i, _count_pos_dir_color) = cond(t2, tuple((u8, readpos(), readdir, u8)))(i)?;

        let t3 = matches!(
            te_type,
            GRENADE_EXPLOSION
                | GRENADE_EXPLOSION_WATER
                | EXPLOSION2
                | PLASMA_EXPLOSION
                | ROCKET_EXPLOSION
                | ROCKET_EXPLOSION_WATER
                | EXPLOSION1
                | EXPLOSION1_NP
                | EXPLOSION1_BIG
                | BFG_EXPLOSION
                | BFG_BIGEXPLOSION
                | BOSSTPORT
                | PLAIN_EXPLOSION
                | CHAINFIST_SMOKE
                | TRACKER_EXPLOSION
                | TELEPORT_EFFECT
                | DBALL_GOAL
                | WIDOWSPLASH
                | NUKEBLAST
        );
        let (i, _pos) = cond(t3, readpos())(i)?;

        let t4 = matches!(
            te_type,
            PARASITE_ATTACK | MEDIC_CABLE_ATTACK | HEATBEAM | MONSTER_HEATBEAM
        );
        let (i, _ent_pos_pos) = cond(t4, tuple((le_i16, readpos(), readpos())))(i)?;

        let (i, _ent_3pos) = cond(
            te_type == GRAPPLE_CABLE,
            tuple((le_i16, readpos(), readpos(), readpos())),
        )(i)?;

        let (i, _2ent_2pos) = cond(
            te_type == LIGHTNING,
            tuple((le_i16, le_i16, readpos(), readpos())),
        )(i)?;

        let (i, _pos_ent) = cond(te_type == FLASHLIGHT, tuple((readpos(), le_i16)))(i)?;

        let (i, _pos2_color) = cond(te_type == FORCEWALL, tuple((readpos(), readpos(), u8)))(i)?;

        let (i, steam) = cond(
            te_type == STEAM,
            tuple((le_i16, u8, readpos(), readdir, u8, le_i16)),
        )(i)?;

        let (i, _time) = cond(matches!(steam, Some((e1,..)) if e1 != -1), le_u32)(i)?;

        let (i, _widow_beam) = cond(te_type == WIDOWBEAMOUT, tuple((le_i16, readpos())))(i)?;

        Ok((i, TempEntity {}))
    }
}
