/// On Server
// pub struct QServerSocket {

// }
use std::net::UdpSocket;

use super::messages::{
    Challenge, ClientConnectionfulMessage, ClientHeader, ClientMessage, ConnectionfulPacketHeader,
    QPort, ServerMessage, StringCmd,
};

#[derive(Default)]
struct ReliableSequenceTracker {
    sequence: u32,
    reliable_oddity_flag: bool,
}

/// On client
pub struct QClientSocket {
    socket: UdpSocket,
    qport: u16,
    buf: [u8; 1500],
    incoming: ReliableSequenceTracker,
    outgoing: ReliableSequenceTracker,
}

impl QClientSocket {
    pub fn new(addr: &str, port: u16) -> anyhow::Result<Self> {
        let s = UdpSocket::bind("0.0.0.0:0")?;

        s.set_broadcast(true).unwrap();
        s.connect((addr, port)).unwrap();
        let mut result = QClientSocket {
            socket: s,
            qport: rand::random(),
            buf: [0; 1500],
            incoming: Default::default(),
            outgoing: Default::default(),
        };
        result.outgoing.sequence = 1;
        result.send_message(ClientMessage::GetChallenge, false)?;

        Ok(result)
    }
    pub fn disconnect(self) {
        todo!()
    }

    fn send_message(&mut self, msg: ClientMessage, increment_sequence: bool) -> anyhow::Result<()> {
        if increment_sequence {
            self.outgoing.sequence += 1;
        }
        // FIXME: do not do this on unreliable messages
        self.outgoing.reliable_oddity_flag = !self.outgoing.reliable_oddity_flag;

        self.socket.send(&msg.serialize())?;
        Ok(())
    }

    pub fn receive_message(&mut self) -> anyhow::Result<ServerMessage> {
        let len = self.socket.recv(&mut self.buf)?;
        let msg = ServerMessage::parse(&self.buf[0..len])?;
        if let ServerMessage::ConnectionfulPacket(header, _) = &msg {
            self.incoming.sequence = header.sequence;

            if header.contains_reliable {
                self.incoming.reliable_oddity_flag = !self.incoming.reliable_oddity_flag;
            }
        }
        Ok(msg)
    }

    pub fn connect(&self, challenge: u32) -> anyhow::Result<()> {
        self.socket
            .send(&ClientMessage::Connect(QPort(self.qport), Challenge(challenge)).serialize())?;
        Ok(())
    }

    pub fn send_new(&mut self) -> anyhow::Result<()> {
        self.send_message(
            ClientMessage::Connectionful(
                self.header(),
                ClientConnectionfulMessage::StringCmd(StringCmd::New),
            ),
            true,
        )?;
        Ok(())
    }

    pub fn forward_to_server(&mut self, cmd: &[u8]) -> anyhow::Result<()> {
        self.send_message(
            ClientMessage::Connectionful(
                self.header(),
                ClientConnectionfulMessage::StringCmd(StringCmd::Raw(cmd)),
            ),
            true,
        )?;
        Ok(())
    }
    fn header(&self) -> ClientHeader {
        ClientHeader {
            // FIXME: support unreliable messages
            sequence_reliable: true,
            sequence: self.outgoing.sequence,
            ack_reliable_oddity_flag: self.incoming.reliable_oddity_flag,
            ack_sequence: self.incoming.sequence,
            qport: self.qport,
        }
    }
}
