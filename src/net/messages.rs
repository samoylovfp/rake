// While developing
#![allow(dead_code)]

use std::{iter::Sum, ops::Shl};

use anyhow::bail;
use bstr::BStr;
use format_bytes::format_bytes;
use glam::Vec3;
use nom::{
    branch::alt,
    bytes::complete::{tag, take, take_till, take_while},
    combinator::{cond, eof, fail, map},
    error::{context, ContextError, ParseError, VerboseError},
    multi::{count, many0, many_till},
    number::complete::{i8, le_i16, le_u16, le_u32, u8},
    sequence::{delimited, preceded, terminated, tuple},
    Finish, IResult, Parser,
};
use nom_derive::{Nom, Parse};

use crate::net::temp_entity::{temp_entity_parser, TempEntity};

#[derive(Debug)]
pub struct Challenge(pub u32);

#[derive(Debug)]
pub struct QPort(pub u16);

const OUT_OF_BOUND: [u8; 4] = (-1_i32).to_le_bytes();
const NET_PROTOCOL_VERSION: u8 = 34;

#[derive(Debug)]
pub struct ClientHeader {
    pub sequence_reliable: bool,
    pub sequence: u32,
    pub ack_reliable_oddity_flag: bool,
    pub ack_sequence: u32,
    pub qport: u16,
}

impl ClientHeader {
    fn serialize(self) -> [u8; 10] {
        let mut result = [0; 10];
        let (left, mid) = result.split_at_mut(4);
        let (mid, right) = mid.split_at_mut(4);

        #[rustfmt::skip]
        left.copy_from_slice(&bit_and_le_u32(
            self.sequence_reliable,
            self.sequence
        ));
        mid.copy_from_slice(&bit_and_le_u32(
            self.ack_reliable_oddity_flag,
            self.ack_sequence,
        ));
        right.copy_from_slice(&self.qport.to_le_bytes());

        result
    }
}

#[derive(Debug)]
pub enum ClientMessage<'data> {
    GetChallenge,
    Connect(QPort, Challenge),
    Connectionful(ClientHeader, ClientConnectionfulMessage<'data>),
}

#[derive(Debug)]
pub enum ClientConnectionfulMessage<'data> {
    StringCmd(StringCmd<'data>),
}

impl<'data> ClientConnectionfulMessage<'data> {
    fn serialize(self) -> Vec<u8> {
        match self {
            ClientConnectionfulMessage::StringCmd(StringCmd::New) => b"\x04new\x00".to_vec(),
            ClientConnectionfulMessage::StringCmd(StringCmd::Raw(r)) => {
                format_bytes!(b"\x04{}\x00", r)
            }
        }
    }
}

#[derive(Debug)]
pub enum StringCmd<'data> {
    New,
    Raw(&'data [u8]),
}

#[derive(Debug)]
pub enum ServerMessage<'packet> {
    Connectionless(ConnectionlessPacket),
    ConnectionfulPacket(
        ConnectionfulPacketHeader,
        Vec<ServerConnectionfulPayload<'packet>>,
    ),
}

#[derive(Debug, Default)]
pub struct ConnectionfulPacketHeader {
    pub contains_reliable: bool,
    pub sequence: u32,
    pub ack_sequence: bool,
    pub ack_flag: u32,
}

#[derive(Debug)]
pub enum ServerConnectionfulPayload<'packet> {
    ServerFrame(ServerFrame<'packet>),
    ServerData(ServerData<'packet>),
    StuffText(&'packet BStr),
    ServerConfigString { index: u16, config: &'packet BStr },
    Unknown,
    CenterPrint(&'packet BStr),
    SpawnBaseLine(EntityState),
    Print(&'packet BStr),
    PlayerInfo(PlayerInfo),
    PacketEntities(Vec<EntityState>),
    MuzzleFlash { entity: i16, f_type: u8 },
    TempEntity(TempEntity),
    Sound(Sound),
    Disconnect,
    Layout(&'packet BStr),
}

#[derive(Debug, Nom)]
#[nom(LittleEndian, GenericErrors)]
pub struct ServerFrame<'packet> {
    frame_number: i32,
    last_frame: i32,
    suppress_count: i8,
    area_bits_length: i8,
    #[nom(Parse = "take(area_bits_length as usize)")]
    area_bits: &'packet [u8],
}

#[derive(Debug, Nom)]
#[nom(LittleEndian, GenericErrors)]
pub struct ServerData<'p> {
    protocol: i32,
    spawn_count: i32,
    demo: i8,
    #[nom(Parse = "null_terminated")]
    game_name: &'p BStr,
    player_number: i16,
    #[nom(Parse = "null_terminated")]
    level_string: &'p BStr,
}

#[derive(Debug)]
pub enum ConnectionlessPacket {
    Challenge(u32),
    ClientConnect,
}

impl<'p> ServerMessage<'p> {
    pub(crate) fn parse(packet_data: &[u8]) -> Result<ServerMessage, anyhow::Error> {
        let last_bit_mask = 1 << 31;
        let bool_and_int = || {
            map(le_u32, move |v| {
                ((v & last_bit_mask) != 0, v & (!last_bit_mask))
            })
        };

        let connectionful_header = tuple((bool_and_int(), bool_and_int())).map(
            |((contains_reliable, sequence), (reliable_sequence, reliable_ack))| {
                ConnectionfulPacketHeader {
                    contains_reliable,
                    sequence,
                    ack_sequence: reliable_sequence,
                    ack_flag: reliable_ack,
                }
            },
        );

        let connectionful = tuple((
            context("conn_header", connectionful_header),
            // many0 because there are ack packages that contain no body
            context(
                "conn_body",
                many0(parse_connectionful_body::<VerboseError<_>>),
            ),
        ));

        let (extra, result) = alt((
            context(
                "connectionless",
                parse_connectionless.map(ServerMessage::Connectionless),
            ),
            context("connectionful", connectionful)
                .map(|(h, b)| ServerMessage::ConnectionfulPacket(h, b)),
        ))
        .parse(packet_data)
        .finish()
        .map_err(|e| anyhow::format_err!("Error parsing server packet: {e:?}"))?;

        if !extra.is_empty() {
            bail!(
                "Successful parse, but extra data.\nParsed {result:?}\n\n Extra\n{:?}",
                extra,
            )
        }

        Ok(result)
    }
}

impl<'d> ClientMessage<'d> {
    pub fn serialize(self) -> Vec<u8> {
        match self {
            ClientMessage::GetChallenge => {
                format_bytes!(b"{}getchallenge", OUT_OF_BOUND)
            }
            ClientMessage::Connect(qport, challenge) => {
                format_bytes!(
                    b"{}connect {} {} {}",
                    OUT_OF_BOUND,
                    NET_PROTOCOL_VERSION,
                    qport.0,
                    challenge.0
                )
            }

            ClientMessage::Connectionful(h, b) => {
                format_bytes!(b"{}{}", h.serialize(), b.serialize())
            }
        }
    }
}

fn bit_and_le_u32(bit: bool, number: u32) -> [u8; 4] {
    let mut result = number.to_le_bytes();
    let mask: u8 = !(1 << 7);
    result[3] &= mask;
    result[3] |= (bit as u8) << 7;
    result
}

fn null_terminated<'d, E: ParseError<&'d [u8]>>(d: &'d [u8]) -> IResult<&[u8], &BStr, E> {
    map(terminated(take_till(|c| c == 0), tag(&[0])), Into::into)(d)
}

pub fn u32_from_ascii(b: &[u8]) -> Option<u32> {
    if b.is_empty() {
        return None;
    }

    let mut result: u32 = 0;

    for byte in b {
        let byte_val = byte - b'0';
        if !matches!(byte_val, 0..=9) {
            return None;
        }
        result = result * 10 + byte_val as u32;
    }
    Some(result)
}

fn parse_connectionless<'d, E>(d: &'d [u8]) -> IResult<&[u8], ConnectionlessPacket, E>
where
    E: ParseError<&'d [u8]> + std::fmt::Debug + ContextError<&'d [u8]>,
{
    let space_or_eof = alt((tag(b" "), eof));
    let digits = take_while(|c: u8| c.is_ascii_digit());

    // Connectionless packet types
    let challenge = delimited(tag(b"challenge "), digits, space_or_eof)
        .map(|s| u32_from_ascii(s).unwrap())
        .map(ConnectionlessPacket::Challenge);
    let client_connect = tag(b"client_connect").map(|_| ConnectionlessPacket::ClientConnect);

    preceded(tag(OUT_OF_BOUND), alt((challenge, client_connect))).parse(d)
}

fn parse_connectionful_body<
    'd,
    E: ParseError<&'d [u8]> + std::fmt::Debug + ContextError<&'d [u8]>,
>(
    d: &'d [u8],
) -> IResult<&[u8], ServerConnectionfulPayload, E> {
    use ServerConnectionfulPayload as P;
    let configstring = tuple((le_u16, null_terminated))
        .map(|(index, config)| P::ServerConfigString { index, config });

    let spawnbaseline = delta_header_parser()
        .flat_map(entity_parser)
        .map(P::SpawnBaseLine);

    let muzzle_flash =
        tuple((le_i16, u8)).map(|(entity, f_type)| P::MuzzleFlash { entity, f_type });

    alt((
        preceded(tag(b"\x01"), muzzle_flash),
        preceded(tag(b"\x03"), temp_entity_parser().map(P::TempEntity)),
        preceded(tag(b"\x04"), null_terminated.map(P::Layout)),
        preceded(tag(b"\x09"), sound().map(P::Sound)),
        preceded(tag(b"\x0a"), null_terminated.map(P::Print)),
        preceded(tag(b"\x0b"), null_terminated.map(P::StuffText)),
        preceded(tag(b"\x0c"), ServerData::parse.map(P::ServerData)),
        preceded(tag(b"\x0d"), configstring),
        preceded(tag(b"\x0e"), spawnbaseline),
        preceded(tag(b"\x0f"), null_terminated.map(P::CenterPrint)),
        preceded(tag(b"\x11"), player_info().map(P::PlayerInfo)),
        preceded(tag(b"\x12"), packet_entities().map(P::PacketEntities)),
        preceded(tag(b"\x14"), ServerFrame::parse.map(P::ServerFrame)),
        tag(b"\x07").map(|_| P::Disconnect),
    ))
    .parse(d)
}

bitflags::bitflags! {
    #[derive(Debug, Clone, Copy)]
    pub struct DeltaHeader: u32 {
        // try to pack the common update flags into the first byte
        const U_ORIGIN1 = (1 << 0);
        const U_ORIGIN2 = (1 << 1);
        const U_ANGLE2 = (1 << 2);
        const U_ANGLE3 = (1 << 3);
        const U_FRAME8 = (1 << 4); // frame is a byte
        const U_EVENT = (1 << 5);
        const U_REMOVE = (1 << 6); // REMOVE this entity, don't add it
        const U_MOREBITS1 = (1 << 7); // read one additional byte

        // second byte
        const U_NUMBER16 = (1 << 8); // NUMBER8 is implicit if not set
        const U_ORIGIN3 = (1 << 9);
        const U_ANGLE1 = (1 << 10);
        const U_MODEL = (1 << 11);
        const U_RENDERFX8 = (1 << 12); // fullbright, etc
        const U_EFFECTS8 = (1 << 14); // autorotate, trails, etc
        const U_MOREBITS2 = (1 << 15); // read one additional byte

        // third byte
        const U_SKIN8 = (1 << 16);
        const U_FRAME16 = (1 << 17); // frame is a short
        const U_RENDERFX16 = (1 << 18); // 8 + 16 = 32
        const U_EFFECTS16 = (1 << 19); // 8 + 16 = 32
        const U_MODEL2 = (1 << 20); // weapons, flags, etc
        const U_MODEL3 = (1 << 21);
        const U_MODEL4 = (1 << 22);
        const U_MOREBITS3 = (1 << 23); // read one additional byte

        // fourth byte
        const U_OLDORIGIN = (1 << 24); // FIXME: get rid of this
        const U_SKIN16 = (1 << 25);
        const U_SOUND = (1 << 26);
        const U_SOLID = (1 << 27);
    }
}

bitflags::bitflags! {
    pub struct SoundFlags: u8 {
        const VOLUME = (1 << 0);
        const ATTENUATION = (1 << 1);
        const POS = (1 << 2);
        const ENT = (1 << 3);
        const OFFSET = (1 << 4);
    }
}

#[derive(Debug)]
pub struct Sound {
    // TODO
}

fn sound<'p, E: ParseError<&'p [u8]>>() -> impl Parser<&'p [u8], Sound, E> {
    |i: &'p [u8]| {
        let (i, flags) = u8(i)?;
        let flags = SoundFlags::from_bits(flags).unwrap();
        let (i, _idx) = u8(i)?;
        let (i, _vol) = cond(flags.contains(SoundFlags::VOLUME), u8)(i)?;
        let (i, _attenuation) = cond(flags.contains(SoundFlags::ATTENUATION), u8)(i)?;
        let (i, _time_offset) = cond(flags.contains(SoundFlags::OFFSET), u8)(i)?;
        let (i, _ent) = cond(flags.contains(SoundFlags::ENT), le_i16)(i)?;
        let (i, _pos) = cond(flags.contains(SoundFlags::POS), count(half(), 3))(i)?;

        Ok((i, Sound {}))
    }
}

pub struct EntityNumber(u16);

fn delta_header_parser<'p, E: nom::error::ParseError<&'p [u8]>>(
) -> impl Parser<&'p [u8], (DeltaHeader, EntityNumber), E> {
    |i: &'p [u8]| {
        let (mut i, mut byte) = u8(i)?;
        let mut header = DeltaHeader::from_bits(u32::from(byte)).unwrap();

        for (offset, continuation_flag) in [
            (8, DeltaHeader::U_MOREBITS1),
            (16, DeltaHeader::U_MOREBITS2),
            (24, DeltaHeader::U_MOREBITS3),
        ] {
            if header.contains(continuation_flag) {
                (i, byte) = u8(i)?;
                header |= DeltaHeader::from_bits(u32::from(byte) << offset).unwrap();
            }
        }
        let wide_entity_number = header.contains(DeltaHeader::U_NUMBER16);
        let entity_number_width = if wide_entity_number { 2 } else { 1 };
        let (i, entity_number) = varint(entity_number_width).parse(i)?;

        Ok((i, (header, EntityNumber(entity_number))))
    }
}

#[derive(Default, Debug)]
pub struct EntityState {
    number: u16,
    model_indices: [u8; 4],
    frame: u16,
    skinnum: u32,
    renderfx: u32,
    origin: Vec3,
    angle: Vec3,
    old_origin: Vec3,
}

fn varint<'p, O, E: nom::error::ParseError<&'p [u8]>>(b: usize) -> impl Parser<&'p [u8], O, E>
where
    O: From<u8> + Shl<usize>,
    O: Sum<<O as Shl<usize>>::Output>,
{
    move |i: &'p [u8]| {
        let (i, bytes) = take(b)(i)?;
        let res = bytes
            .into_iter()
            .copied()
            .map(O::from)
            .enumerate()
            .map(|(i, b)| b << (i * 8))
            .sum();
        Ok((i, res))
    }
}

fn entity_parser<'p, E: nom::error::ParseError<&'p [u8]>>(
    (header, EntityNumber(n)): (DeltaHeader, EntityNumber),
) -> impl Parser<&'p [u8], EntityState, E> {
    move |mut i: &'p [u8]| {
        let single_byte = || map(take(1_usize), |b: &[u8]| b[0]);

        let number_width = |h: &DeltaHeader, b8: DeltaHeader, b16: DeltaHeader| match (
            h.contains(b8),
            h.contains(b16),
        ) {
            (true, true) => 4,
            (false, true) => 2,
            (true, false) => 1,
            (false, false) => 0,
        };

        let mut e = EntityState::default();
        e.number = n;

        for (idx, flag) in [
            DeltaHeader::U_MODEL,
            DeltaHeader::U_MODEL2,
            DeltaHeader::U_MODEL3,
            DeltaHeader::U_MODEL4,
        ]
        .into_iter()
        .enumerate()
        {
            // FIXME: use `cond` combinator
            if header.contains(flag) {
                (i, e.model_indices[idx]) = single_byte()(i)?;
            }
        }

        // This seems to be an invariant
        assert!(
            !(header.contains(DeltaHeader::U_FRAME8) && header.contains(DeltaHeader::U_FRAME16)),
            "Frame8 and Frame16 should not be set at the same time"
        );

        let frame_width = number_width(&header, DeltaHeader::U_FRAME8, DeltaHeader::U_FRAME16);
        (i, e.frame) = varint(frame_width).parse(i)?;

        let skin_width = number_width(&header, DeltaHeader::U_SKIN8, DeltaHeader::U_SKIN16);
        (i, e.skinnum) = varint(skin_width).parse(i)?;

        let effects_width =
            number_width(&header, DeltaHeader::U_EFFECTS8, DeltaHeader::U_EFFECTS16);
        let (mut i, _effects) = varint::<u32, _>(effects_width).parse(i)?;

        let renderfx_width =
            number_width(&header, DeltaHeader::U_RENDERFX8, DeltaHeader::U_RENDERFX16);
        (i, e.renderfx) = varint(renderfx_width).parse(i)?;

        let angle = || map(single_byte(), |b| b as f32 * 180.0 / 128.0);

        for (idx, orig_component) in [
            DeltaHeader::U_ORIGIN1,
            DeltaHeader::U_ORIGIN2,
            DeltaHeader::U_ORIGIN3,
        ]
        .into_iter()
        .enumerate()
        {
            if header.contains(orig_component) {
                (i, e.origin[idx]) = half().parse(i)?;
            }
        }

        for (idx, ang_component) in [
            DeltaHeader::U_ANGLE1,
            DeltaHeader::U_ANGLE2,
            DeltaHeader::U_ANGLE3,
        ]
        .into_iter()
        .enumerate()
        {
            if header.contains(ang_component) {
                (i, e.angle[idx]) = angle()(i)?;
            }
        }

        if header.contains(DeltaHeader::U_OLDORIGIN) {
            (i, e.old_origin[0]) = half().parse(i)?;
            (i, e.old_origin[1]) = half().parse(i)?;
            (i, e.old_origin[2]) = half().parse(i)?;
        }
        let _sound;
        if header.contains(DeltaHeader::U_SOUND) {
            (i, _sound) = take(1_usize)(i)?;
        }

        if header.contains(DeltaHeader::U_EVENT) {
            let _event;
            (i, _event) = take(1_usize)(i)?;
        }
        if header.contains(DeltaHeader::U_SOLID) {
            let _solid;
            (i, _solid) = take(2_usize)(i)?;
        }

        Ok((i, e))
    }
}

bitflags::bitflags! {
    pub struct PlayerInfoFlags: u16 {
        const PS_M_TYPE = (1 << 0);
        const PS_M_ORIGIN = (1 << 1);
        const PS_M_VELOCITY = (1 << 2);
        const PS_M_TIME = (1 << 3);
        const PS_M_FLAGS = (1 << 4);
        const PS_M_GRAVITY = (1 << 5);
        const PS_M_DELTA_ANGLES = (1 << 6);
        const PS_VIEWOFFSET = (1 << 7);

        const PS_VIEWANGLES = (1 << 8);
        const PS_KICKANGLES = (1 << 9);
        const PS_BLEND = (1 << 10);
        const PS_FOV = (1 << 11);
        const PS_WEAPONINDEX = (1 << 12);
        const PS_WEAPONFRAME = (1 << 13);
        const PS_RDFLAGS = (1 << 14);
    }
}

#[derive(Debug)]
pub struct PlayerInfo {
    //TODO
}

fn half<'p, E: ParseError<&'p [u8]>>() -> impl Parser<&'p [u8], f32, E> {
    le_i16.map(|int| (int as f32) / 8.0)
}

fn player_info<'p, E: nom::error::ParseError<&'p [u8]>>() -> impl Parser<&'p [u8], PlayerInfo, E> {
    use PlayerInfoFlags as PIF;
    |i: &'p [u8]| {
        let (i, header) = le_u16(i)?;
        let header = PlayerInfoFlags::from_bits(header).unwrap();
        let quart = || i8.map(|n| n as f32 * 0.25);
        let short_angle = le_i16.map(|v| v as f32 * 180.0 / (i16::MAX as f32));

        let (i, _move_type) = cond(header.contains(PIF::PS_M_TYPE), u8)(i)?;
        let (i, _move_origin) = cond(header.contains(PIF::PS_M_ORIGIN), count(le_i16, 3))(i)?;
        let (i, _move_velocity) = cond(header.contains(PIF::PS_M_VELOCITY), count(le_i16, 3))(i)?;
        let (i, _move_time) = cond(header.contains(PIF::PS_M_TIME), u8)(i)?;
        let (i, _move_flags) = cond(header.contains(PIF::PS_M_FLAGS), u8)(i)?;
        let (i, _move_gravity) = cond(header.contains(PIF::PS_M_GRAVITY), le_i16)(i)?;
        let (i, _move_delta_angles) =
            cond(header.contains(PIF::PS_M_DELTA_ANGLES), count(le_i16, 3))(i)?;
        let (i, _view_offset) = cond(header.contains(PIF::PS_VIEWOFFSET), count(quart(), 3))(i)?;
        let (i, _view_angles) =
            cond(header.contains(PIF::PS_VIEWANGLES), count(short_angle, 3))(i)?;
        let (i, _kick_angles) = cond(header.contains(PIF::PS_KICKANGLES), count(quart(), 3))(i)?;
        let (i, _weap_idx) = cond(header.contains(PIF::PS_WEAPONINDEX), u8)(i)?;
        let (i, _weap_frame) = cond(
            header.contains(PIF::PS_WEAPONFRAME),
            tuple((u8, count(quart(), 3), count(quart(), 3))),
        )(i)?;

        let byte_float = || u8.map(|n| n as f32 / 255.0);

        let (i, _blend) = cond(header.contains(PIF::PS_BLEND), count(byte_float(), 4))(i)?;
        let (i, _fov) = cond(header.contains(PIF::PS_FOV), u8)(i)?;
        let (i, _rd_flags) = cond(header.contains(PIF::PS_RDFLAGS), u8)(i)?;
        let (i, statbits) = le_u32(i)?;
        let mut i = i;
        for stat_n in 0..32 {
            if (statbits & (1 << stat_n)) != 0 {
                let mut _stat;
                (i, _stat) = le_i16(i)?;
            }
        }

        Ok((i, PlayerInfo {}))
    }
}

fn packet_entities<'p, E: ParseError<&'p [u8]>>() -> impl Parser<&'p [u8], Vec<EntityState>, E> {
    let zero_num_header = |i| {
        let (i, (_header, EntityNumber(n))) = delta_header_parser().parse(i)?;
        if n == 0 {
            Ok((i, ()))
        } else {
            fail(i)
        }
    };
    many_till(
        delta_header_parser().flat_map(entity_parser),
        zero_num_header,
    )
    .map(|(e, _)| e)
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;

    use nom::{
        bytes::complete::tag, error::VerboseError, multi::many1, sequence::preceded, IResult,
        Parser,
    };
    use nom_derive::Parse;
    use pcap_file::pcapng::{Block, PcapNgReader};
    use pdu::{Ethernet, EthernetPdu, Ipv4, Udp};
    use test_case::test_case;

    use crate::net::messages::{packet_entities, parse_connectionful_body};

    use super::{
        bit_and_le_u32, delta_header_parser, entity_parser, null_terminated, u32_from_ascii,
        varint, ServerData, ServerMessage,
    };

    #[test]
    fn parse_challenge_smoke() {
        let payload = [
            255, 255, 255, 255, 99, 104, 97, 108, 108, 101, 110, 103, 101, 32, 51, 49, 52, 48, 48,
            32,
        ];

        let packet = ServerMessage::parse(payload.as_slice()).unwrap();
        insta::assert_debug_snapshot!(packet, @r###"
        Connectionless(
            Challenge(
                31400,
            ),
        )
        "###);
    }

    #[test]
    fn test_command() {
        let mut pcap =
            PcapNgReader::new(Cursor::new(include_bytes!("../../q2pro_to_steam.pcapng"))).unwrap();
        let mut packet_n = 0;
        let mut parsed_packets = vec![];
        while let Some(p) = pcap.next_block() {
            let p = p.unwrap();
            let Block::EnhancedPacket(p) = p else { println!("{p:?}"); continue };
            packet_n += 1;

            let p = EthernetPdu::new(&p.data).unwrap();
            let Ethernet::Ipv4(p) = p.inner().unwrap() else {panic!("Non-ipv4: {p:?}")};
            let Ipv4::Udp(p) = p.inner().unwrap() else {continue};
            if p.source_port() == 27910 {
                let Udp::Raw(data) = p.inner().unwrap();
                let res = ServerMessage::parse(data);

                parsed_packets.push(format!("{packet_n} {:?}", res));
            }
        }

        insta::assert_debug_snapshot!(parsed_packets);
    }

    #[test]
    fn test_serverdata() {
        let serverdata_packet = [
            1, 0, 0, 128, 1, 0, 0, 128, 12, 34, 0, 0, 0, 157, 87, 0, 0, 0, 0, 1, 0, 84, 104, 101,
            32, 69, 100, 103, 101, 0, 11, 99, 109, 100, 32, 99, 111, 110, 102, 105, 103, 115, 116,
            114, 105, 110, 103, 115, 32, 50, 50, 52, 50, 57, 32, 48, 10, 0,
        ];
        let parsed = ServerMessage::parse(serverdata_packet.as_slice()).unwrap();
        insta::assert_debug_snapshot!(parsed, @r###"
        ConnectionfulPacket(
            ConnectionfulPacketHeader {
                contains_reliable: true,
                sequence: 1,
                ack_sequence: true,
                ack_flag: 1,
            },
            [
                ServerData(
                    ServerData {
                        protocol: 34,
                        spawn_count: 22429,
                        demo: 0,
                        game_name: "",
                        player_number: 1,
                        level_string: "The Edge",
                    },
                ),
                StuffText(
                    "cmd configstrings 22429 0\n",
                ),
            ],
        )
        "###);
    }
    #[test]
    fn test_nul_terminated() {
        insta::assert_debug_snapshot!(null_terminated::<VerboseError<_>>(b"123\x00"), @r###"
        Ok(
            (
                [],
                "123",
            ),
        )
        "###);
    }

    #[test]
    fn test_serverdata_inner() {
        let serverdata_inner = [
            34, 0, 0, 0, 157, 87, 0, 0, 0, 0, 1, 0, 84, 104, 101, 32, 69, 100, 103, 101, 0,
        ];
        let res: IResult<_, _, VerboseError<_>> = ServerData::parse(&serverdata_inner);
        insta::assert_debug_snapshot!(res, @r###"
        Ok(
            (
                [],
                ServerData {
                    protocol: 34,
                    spawn_count: 22429,
                    demo: 0,
                    game_name: "",
                    player_number: 1,
                    level_string: "The Edge",
                },
            ),
        )
        "###)
    }

    #[test_case(b"123", Some(123))]
    #[test_case(b"", None)]
    #[test_case(b"0", Some(0))]
    #[test_case(b"1234122q", None)]
    fn test_i32_parsing(input: &[u8], result: Option<u32>) {
        assert_eq!(u32_from_ascii(input), result)
    }

    #[test_case(2, true, b"\x02\x00\x00\x80")]
    #[test_case(400, false, b"\x90\x01\x00\x00")]
    #[test_case(67232323, true, b"\x43\xe2\x01\x84")]
    fn test_quake_bit_shenanigans(number: u32, bit: bool, result: &[u8; 4]) {
        assert_eq!(&bit_and_le_u32(bit, number), result)
    }

    #[test]
    fn test_varint_8() {
        let res: IResult<_, _, VerboseError<_>> = varint(1).parse(&[1]);
        assert_eq!(res, Ok(([].as_slice(), 1_u8)))
    }

    #[test]
    fn test_varint_16() {
        let res: IResult<_, _, VerboseError<_>> = varint(2).parse(&[1, 2]);
        assert_eq!(res, Ok(([].as_slice(), 513_u16)))
    }

    #[test]
    fn test_varint_24() {
        let res: IResult<_, _, VerboseError<_>> = varint(3).parse(&[1, 2, 3]);
        assert_eq!(res, Ok(([].as_slice(), 197121_u32)))
    }

    #[test]
    fn test_delta_parsing1() {
        let data = include!("delta_entity_bytes_with_extra.array");
        let mut parser = many1(preceded(
            tag(&[14]),
            delta_header_parser::<VerboseError<_>>().flat_map(entity_parser),
        ));
        let (extra, result) = parser.parse(&data).unwrap();
        insta::assert_debug_snapshot!((extra, result));
    }

    #[test]
    fn test_delta_parsing2() {
        let data = include!("delta_entity_bytes2.array");
        let mut parser = delta_header_parser::<VerboseError<_>>().flat_map(entity_parser);

        let r = format!("{:?}", parser.parse(&data).unwrap());
        insta::assert_debug_snapshot!(r);
    }

    #[test]
    fn test_delta_parsing3() {
        let data = [144, 128, 128, 1, 2, 11, 182, 58, 193, 13, 1, 29, 0, 0];
        insta::assert_debug_snapshot!(packet_entities::<VerboseError<_>>().parse(&data))
    }

    #[test]
    fn test_layout_message() {
        let data = include!("layout_bytes.array");
        insta::assert_debug_snapshot!(parse_connectionful_body::<VerboseError<_>>(&data));
    }
}
