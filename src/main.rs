use assert_matches::assert_matches;
use format_bytes::format_bytes;
use std::net::UdpSocket;

use rake::net::messages::ServerConnectionfulPayload;
use rake::net::{ClientMessage, ConnectionlessPacket, QClientSocket, ServerMessage};

fn main() {
    let mut client_socket = QClientSocket::new("127.0.0.1", 27910).unwrap();

    let ServerMessage::Connectionless(ConnectionlessPacket::Challenge(challenge)) = client_socket.receive_message().unwrap() else {
        panic!("did not receive challenge")
    };

    client_socket.connect(challenge).unwrap();

    assert_matches!(
        client_socket.receive_message().unwrap(),
        ServerMessage::Connectionless(ConnectionlessPacket::ClientConnect)
    );

    client_socket.send_new().unwrap();

    // read configstrings and baseline
    loop {
        let message = client_socket.receive_message().unwrap();
        let mut response = vec![];
        if let ServerMessage::ConnectionfulPacket(_h, sub_messages) = message {
            for sm in sub_messages {
                if let ServerConnectionfulPayload::StuffText(st) = sm {
                    if let Some(cmd) = st.strip_prefix(b"cmd ") {
                        let cmd = cmd.strip_suffix(b"\n").unwrap_or(cmd).to_owned();
                        response.push(cmd);
                    } else if let Some(e) = st.strip_prefix(b"precache ") {
                        response.push(format_bytes!(b"begin {}", e));
                    }
                }
            }
        }
        for r in response {
            client_socket.forward_to_server(&r).unwrap();
        }
    }
}
